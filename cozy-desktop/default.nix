# Remaining issues :
# * the `Open Folder` and `Open Cozy` buttons do nothing
# * icons are not displayed in location chooser window
# * application in the menu doesn't work

{ appimageTools
, fetchurl
, lib
, gsettings-desktop-schemas
, gtk3
, hostPlatform
}:

let
  pname = "cozy-desktop";
  version = "3.26.1";
  shortDescription = "File Synchronisation for Cozy on Desktop and Laptop";
  sha256 = {
    x86_64-linux = "0793j49k8ah0hzg4gcx94zm5yycaigs26rg5cfz8js3g464aa9nw";
    i386-linux = "1a3f4sh4xvi09c178jc9zji167c9220psbrdw14p602xsa243n7f";
    i686-linux = "1a3f4sh4xvi09c178jc9zji167c9220psbrdw14p602xsa243n7f";
  }.${hostPlatform.system};

  name = "${pname}-${version}";

  architecture = {
    x86_64-linux = "x86_64";
    i386-linux = "i386";
    i686-linux = "i386";
  }.${hostPlatform.system};

  src = fetchurl {
    url = "https://github.com/cozy-labs/cozy-desktop/releases/download/v${version}/Cozy-Drive-${version}-${architecture}.AppImage";
    inherit sha256;
  };
in
appimageTools.wrapType2 rec {
  inherit name src;

  profile = ''
    # The following variable fix the following error:
    # > (cozydrive-bin:2): GLib-GIO-ERROR **: 16:11:52.088: No GSettings schemas are installed on the system
    # When installing, at the step :
    # > Select a location for your Cozy folder:
    # When choosing a non-default location
    # There is still an issue with icons in location chooser window
    export XDG_DATA_DIRS="${gsettings-desktop-schemas}/share/gsettings-schemas/${gsettings-desktop-schemas.name}:${gtk3}/share/gsettings-schemas/${gtk3.name}:$XDG_DATA_DIRS"
  '';

  extraInstallCommands = ''
    # Strip version from binary name
    mv $out/bin/${name} $out/bin/${pname}
  '';

  meta = {
    description = shortDescription;
    longDescription = ''
      Cozy Drive for Desktop allows you to synchronize the files stored in your Cozy with your laptop and/or desktop computer. It replicates your files on your hard drive and apply changes you made on them on other synced devices and on your online Cozy.
    '';
    homepage = "https://cozy-labs.github.io/cozy-desktop/";
    downloadPage = "https://github.com/cozy-labs/cozy-desktop/releases";
    license = lib.licenses.agpl3Only;
    platforms = [ "x86_64-linux" "i386-linux" "i686-linux" ];
  };
}

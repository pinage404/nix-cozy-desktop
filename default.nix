{ pkgs ? import <nixpkgs> {} }:

let
  cozy-desktop = pkgs.callPackage ./cozy-desktop {};
in
pkgs.mkShell {
  buildInputs = [
    cozy-desktop
  ];
}

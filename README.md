# Cozy Desktop wrapper for NixOS

Installation can be done at different levels, see below

You will have to run command several times to change the `sha256` by the given one by Nix

## Home-Manager / user level

Install [Home Manager](https://nix-community.github.io/home-manager/)

In `~/.config/nixpkgs/home.nix`

```nix
{ pkgs, ... }:

let
  cozy-desktop-derivation = pkgs.fetchurl {
    url = "https://gitlab.com/pinage404/nix-cozy-desktop/-/raw/main/cozy-desktop/default.nix";
    sha256 = "0000000000000000000000000000000000000000000000000000";
  };
  cozy-desktop = pkgs.callPackage cozy-desktop-derivation {};
in
{
  home.packages = [
    cozy-desktop

    # others dependencies here
    pkgs.gitAndTools.git-absorb
  ];
}
```

Then run this command

```shell
home-manager switch
```

## NixOS / system level

In `/etc/nixos/configuration.nix`

```nix
{ pkgs, ... }:

let
  cozy-desktop-derivation = pkgs.fetchurl {
    url = "https://gitlab.com/pinage404/nix-cozy-desktop/-/raw/main/cozy-desktop/default.nix";
    sha256 = "0000000000000000000000000000000000000000000000000000";
  };
  cozy-desktop = pkgs.callPackage cozy-desktop-derivation {};
in
{
  environment.systemPackages = [
    cozy-desktop

    # others dependencies here
    pkgs.bat
  ];

  # This value determines the NixOS release with which your system is to be compatible, in order to avoid breaking some software such as database servers
  # You should change this only after NixOS release notes say you should
  system.stateVersion = "20.09";
}
```

Then run this command

```shell
nixos-rebuild switch
```
